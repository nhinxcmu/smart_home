import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'components/sAppBar.dart';
import 'components/sRoom.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
      theme: ThemeData(primarySwatch: Colors.blue),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: IconButton(
          color: Colors.black87,
          onPressed: () {},
          icon: Icon(Icons.add),
        ),
        backgroundColor: Colors.white,
        
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SAppBar(
            title: "Smart Home",
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: GridView.count(
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                crossAxisCount: 2,
                children: <Widget>[
                  SRoom(
                    image: "assets/images/room-living.png",
                    soLuongThietBi: 3,
                    tenPhong: "Phòng khách",
                  ),
                  SRoom(
                    image: "assets/images/room-bath.png",
                    soLuongThietBi: 5,
                    tenPhong: "Phòng tắm",
                  ),
                  SRoom(
                    image: "assets/images/room-bed.png",
                    soLuongThietBi: 3,
                    tenPhong: "Phòng ngủ bố mẹ",
                  ),
                  SRoom(
                    image: "assets/images/room-bed.png",
                    soLuongThietBi: 5,
                    tenPhong: "Phòng ngủ con",
                  ),
                  SRoom(
                    image: "assets/images/room-dinning.png",
                    soLuongThietBi: 5,
                    tenPhong: "Nhà bếp",
                  ),
                  SRoom(
                    image: "assets/images/room-yard.png",
                    soLuongThietBi: 5,
                    tenPhong: "Sân vườn",
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
