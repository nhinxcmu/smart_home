import 'package:flutter/material.dart';

class SAppBar extends StatefulWidget {
  SAppBar({Key key, this.title}) : super(key: key);
  final String title;
  _SAppBarState createState() => _SAppBarState();
}

class _SAppBarState extends State<SAppBar> {
  openDrawer() {
    Scaffold.of(context).openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              child: IconButton(
                onPressed: openDrawer,
                icon: Icon(Icons.menu),
              ),
            ),
          ),
          Expanded(
            flex: 6,
            child: Container(
              alignment: Alignment.center,
              child: Text(widget.title,style: Theme.of(context).textTheme.title,),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: IconButton(
                onPressed: (){},
                icon: Icon(Icons.notifications_none),
              ),
            ),
          )
        ],
      ),
    );
  }
}
