import 'package:flutter/material.dart';

class SRoom extends StatefulWidget {
  SRoom({Key key, this.image, this.soLuongThietBi, this.tenPhong})
      : super(key: key);
  String image;
  String tenPhong;
  int soLuongThietBi;
  _SRoomState createState() => _SRoomState();
}

class _SRoomState extends State<SRoom> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 200) / 2,
      height: (MediaQuery.of(context).size.width - 200) / 2,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: [
          BoxShadow(
              blurRadius: 10,
              color: Color.fromRGBO(0, 85, 128, 0.15),
              offset: Offset(0, 2))
        ],
      ),
      child: Material(
        child: InkWell(
          onTap: () {
            // Scaffold.of(context).showSnackBar(SnackBar(
            //   content: Text("Nhi"),
            // ));
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                    child: Image.asset(
                  widget.image,
                  scale: 2,
                )),
              ),
              Text(
                widget.tenPhong,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 16),
              ),
              Text(
                "${widget.soLuongThietBi} thiết bị",
                style: TextStyle(fontSize: 12, color: Colors.black87),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
